# O1A 

Elementary Programming skills.

An innovative online course in programming for students without mathematical or IT background. 12 introductory lessons, each one formulated in an easy to understand way for the non-IT students/specialists.

Of which, University of Beds. is responsible for 1 lesson which will be focussing on pandas a library for Python. This elementary lesson will be an introduction to O1B. This O1A lesson will be specifically designed for the integration of statistics with Python for the following University degree subject areas: economics, business and social sciences.