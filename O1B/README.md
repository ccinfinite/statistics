# O1B

Specialised methods for non-IT arts and sciences.

This project will consist of 6 lessons. The first three will be completed by UoB, these include: Input and Output, Manipulation of Data and Graphs. The remaining three will be developed by our Spanish partners, however, we will still be brainstorming ideas for these lessons, which include: Descriptive Statistics, Probability Distributions and Correlation, hypothesis testing, forecasting, etc.

Each lesson will have 5 questions which will accompany the content.