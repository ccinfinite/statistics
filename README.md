# CCI-UoB

***This is the repository for the developement of University of Bedfordshire's part in the CCI project. There is two parts to this project: the O1A lesson and the 6 O1B lessons.***

-------

# O1A
Elementary Programming skills.

An innovative online course in programming for students without mathematical or IT background. 12 introductory lessons, each one formulated in an easy to understand way for the non-IT students/specialists.

Of which, University of Beds. is responsible for 1 lesson which will be focussing on pandas a library for Python. This elementary lesson will be an introduction to O1B. This O1A lesson will be specifically designed for the integration of statistics with Python for the following University degree subject areas: economics, business and social sciences.


-------

# O1B

Specialised methods for non-IT arts and sciences.

This project will consist of 6 lessons. The first three will be completed by UoB, these include: Input and Output, Manipulation of Data and Graphs. The remaining three will be developed by our Spanish partners, however, we will still be brainstorming ideas for these lessons, which include: Descriptive Statistics, Probability Distributions and Correlation, hypothesis testing, forecasting, etc.

Each lesson will have 5 questions which will accompany the content.
